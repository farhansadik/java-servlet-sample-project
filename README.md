# Create a new Project from scratch 
The Sun Microsystem defines a unique standard to be followed by all the server vendors. Let's see the directory structure that must be followed to create the servlet.

```
web-app(context-root)
	|----WEB-INF
	|		|----classes(classes files)
	|		|----web.xml
	|		|----lib
	|----HTML
	|----Static Resources (images, css, etc)
```

### Intellij Idea 
1. File > New > Project 
	a. Build System `Gradle`
	b. Gradle DSL `Groovy`
2. Follow same structure as this method...
```
├── build.gradle
├── gradle
│   └── wrapper
│       ├── gradle-wrapper.jar
│       └── gradle-wrapper.properties
├── gradlew
├── gradlew.bat
├── settings.gradle
└── src
    ├── main
    │   ├── java
    │   │   └── com
    │   │       └── farhan
    │   │           └── orca
    │   │               └── time.java
    │   ├── resources
    │   └── webapp
    │       ├── index.jsp
    │       └── WEB-INF
    │           └── web.xml
    └── test
        ├── java
        └── resources
```

web.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app>
    <welcome-file-list>
        <welcome-file>index.jsp</welcome-file>
    </welcome-file-list>

    <!-- sample servlet mapping
    <servlet-mapping>
        <servlet-name>Server Time</servlet-name>
        <url-pattern>/server-time</url-pattern>
    </servlet-mapping>
   -->
</web-app>
```
index.jsp
```jsp
<% response.sendRedirect("home"); %>
```
build.gradle
```gradle
plugins {
    id 'java'
    id 'war'
}

group 'com.farhan.orca'
version '0.1_alpha'

dependencies {
    compileOnly 'javax.servlet:javax.servlet-api:4.0.1'
}
```

For **Gralde changess CTRL+SHIFT+O** 

Sample Project <https://gitlab.com/farhansadik/java-servlet-sample-project>

<https://www.javatpoint.com/steps-to-create-a-servlet-using-tomcat-server>
